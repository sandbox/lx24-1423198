<?php

/**
 * @file
 * Integration with Views.
 */

/**
 * Implements hook_views_plugin().
 */
function css_grid_views_plugins() {
  $plugins['style']['css_grid'] = array(
    'title' => t('CSS Grid'),
    'help' => t('Displays rows in a css styled grid.'),
    'handler' => 'css_grid_style_plugin',
    'theme' => 'css_grid_view',
    'uses row plugin' => TRUE,
    'uses options' => TRUE,
    'uses grouping' => FALSE,
    'type' => 'normal',
  );
  return $plugins;
}